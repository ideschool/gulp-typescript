const gulp = require("gulp");
const ts = require("gulp-typescript");
const tsProject = ts.createProject("tsconfig.json");
const browserSync = require('browser-sync').create();

gulp.task("ts", () => {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("dist"));
});

gulp.task("html", () => {
    return gulp.src("src/index.html")
        .pipe(gulp.dest("dist"))
});

gulp.task("ts-watch", () => {
    gulp.watch("src/**/*.*", ["ts"]);
});

gulp.task("html-watch", () => {
    gulp.watch("src/index.html", ["html"]);
});

gulp.task('serve', ['ts-watch', "html-watch"], function() {

    browserSync.init({
        server: "dist"
    });

    gulp.watch("dist/*.js").on('change', browserSync.reload);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});
